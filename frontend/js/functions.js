
var orderColumn = "name";
var orderType = "ASC";
var page = 1;

var tableOffset = header = fixedHeader = null;

getContacts();

$( document ).ready(function() {
	
	$( "#btn-search" ).click(function() {
		page = 1;
		getContacts();
	});

	$( ".column-order" ).click(function(e) {		
		orderBy(this);
		getContacts();
	});

	$( "#txt-textFilter" ).keypress(function(event) {
		if ( event.which == 13 )
			getContacts();
	});

	$( window ).scroll(function() {
		var offset = $(this).scrollTop();

		if (offset >= tableOffset && $fixedHeader.is(":hidden"))
			$fixedHeader.show();

		else if (offset < tableOffset)
			$fixedHeader.hide();

		var columnsFirst = $("#table-contacts > thead > tr > th");
		var columnsClone = $("#header-fixed > thead > tr > th");

		for (var i = 0; i < columnsFirst.length; i++) {
			columnFirst = columnsFirst[i];
			columnClone = columnsClone[i];
			$(columnClone).first().width( $(columnFirst).first().width() );
		}

	});

	cloneHeadTable();

});

function cloneHeadTable()
{
	tableOffset = $("#table-contacts").offset().top;
	$header = $("#table-contacts > thead").clone();
	$fixedHeader = $("#header-fixed").append($header);
}

function orderBy(obj)
{
	obj = obj;
	var orderColumnNew = $( obj ).data( "column");

	$(obj).parent().find("span").removeClass("glyphicon-chevron-down");
	$(obj).parent().find("span").removeClass("glyphicon-chevron-up");

	if(orderColumn == orderColumnNew)
	{
		if(orderType == "ASC"){
			orderType = "DESC";
			$(obj).children("span").first().removeClass("glyphicon-chevron-down");
			$(obj).children("span").first().addClass("glyphicon-chevron-up");
		}
		else{
			orderType = "ASC";
			$(obj).children("span").first().removeClass("glyphicon-chevron-up");
			$(obj).children("span").first().addClass("glyphicon-chevron-down");			
		}
	}
	else
	{
		orderColumn = orderColumnNew;
		$(obj).children("span").first().addClass("glyphicon-chevron-down");
	}
	
}

function getContacts()
{
	var textFilter = $("#txt-textFilter").val();
	var typeFilter = $("#select-typeFilter").val();	

	$.get( "import.php", { 
		orderColumn: orderColumn, page: page, textFilter: textFilter, typeFilter: typeFilter, orderType: orderType
		})
	.done(function( data ) {
		$("#table-contacts > tbody").html("");
		showContacts(data.contacts);
		showPagination(data.pageTotal, data.page);
		loadEvent();
	});
}

function loadEvent()
{
	$( ".img-avatar" ).click(function(e) {
		e.preventDefault();
		getAvatar(this);
	});

	$( ".pagination-number" ).click(function(e) {
		e.preventDefault();
		page = $( this ).data( "page");
		getContacts();
	});
}

function showPagination(total = 1, page = 1)
{
	$("#pagination > ul").html("");
	$("#pagination ul").append('<li><a class="pagination-number" data-page="1" href="#" aria-label="Next"><span aria-hidden="true">&laquo;</span></a></li>');
	
	for (var i = 1; i <= total; i++) {
		if(i == page)
			$("#pagination ul").append('<li class="active"><a class="pagination-number" data-page="' + i +'" href="#">' + i + '</a></li>');
		else
			$("#pagination ul").append('<li><a class="pagination-number" data-page="' + i +'" href="#">' + i + '</a></li>');
	}

	$("#pagination ul").append('<li><a class="pagination-number" data-page="' + total + '" href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>');
}

function getAvatar(obj)
{
	var id = $( obj ).data( "id");
	var imgAvatar = $(obj).attr("src");	
	$("#img-avatar").attr("src", imgAvatar);
	$("#modal-img-avatar").modal("show");	
}

function showContacts(contacts)
{
	for (var i = 0; i < contacts.length; i++)
		showContact(contacts[i], i+1 + (page*10-10));
}

function showContact(contacts, number)
{
	var row = "";

	row = "<tr>";
	row += "<td>" + number + "</td>";
	row += "<td>" + contacts.name + "</td>";
	row += "<td>" + contacts.last_name + "</td>";
	row += "<td>" + contacts.address + "</td>";
	row += "<td>" + contacts.cellphone + "</td>";
	row += "<td>" + contacts.telephone + "</td>";	
	row += "<td><img data-id='" + contacts.ID + "' class='img-avatar' src='" + contacts.avatar + "' height='40'></td>";	
	row += "</tr>";

	$('#table-contacts').append(row);
}
