<?php
/**
 * Crea las clases que creas necesarias para hacer el trabajo.
 * Ten en cuenta que el archivo `users.csv`
 */

/*
* Eswin Palacios Sarmiento
* 2017
*/

class Import{

	var $listPersons;
	var $db;
	var $rowsCount = 0;

	function __construct() {
		$this->conection();
   	}

	public function createTable()
	{
		$sql = "DROP TABLE users";
		$this->db->exec($sql);

		$sql = "CREATE TABLE users(ID INTEGER PRIMARY KEY AUTOINCREMENT,
		name TEXT,
		last_name TEXT,
		address TEXT,
		cellphone TEXT,
		telephone TEXT,
		avatar TEXT)";

		$this->db->exec($sql);
	}

	private function conection()
	{
		$this->db = require __DIR__.'../db/db.php';
	}

	private function insertData($obj)
	{
		$obj->last_name = str_replace("'", "\''", $obj->last_name);
		$obj->name = str_replace("'", "\''", $obj->name);

		$sql = "('$obj->name', '$obj->last_name', '$obj->address', '$obj->cellphone', '$obj->telephone', '$obj->avatar')";

		return $sql;
	}

	public function showData()
	{
		$sql = "SELECT * FROM users";

		$ret = $this->db->query($sql);
		$rows = $ret->fetchAll();

		print_r($rows);
	}

	private function insertDatas()
	{
		$list = $this->listPersons;
		$sql = "";
		
		for ($i=0; $i < count($list); $i++)
		{	
			if($i%1000 == 0){
				if($sql != "")
					$result = $this->db->exec($sql);

				$sql = "\nINSERT into users (name, last_name, address, cellphone, telephone, avatar) values ";
			}
			else
				$sql .= ",";
			
			$sql .= $this->insertData($list[$i]);
		}
		
		if($sql != "")
			$result = $this->db->exec($sql);

	}

	private function processData($row)
	{
		$objPerson = new Person();
		$fields = explode("|", $row);
		$numbers = explode("-", $fields[3]);

		$objPerson->name = $fields[0];
		$objPerson->last_name = $fields[1];
		$objPerson->address = $fields[2];
		$objPerson->cellphone = $numbers[0];
		$objPerson->telephone = $numbers[1];
		$objPerson->avatar = $fields[4];

		return $objPerson;
	}

	private function readSource($fileName)
	{
		$rowCount = 1;
		$this->listPersons = [];
		$sql = "";
		
		if (($file = fopen($fileName, "r")) !== FALSE) {
			while (($data = fgetcsv($file, 1000, ",")) !== FALSE) {
				
				if($rowCount>1){
					$objPerson = $this->processData($data[0]);
					array_push($this->listPersons, $objPerson);
				}
				
				$rowCount++;
			}
			fclose($file);
		}
		
		$this->insertDatas();
	}

	public function importData($fileName)
	{
		$this->readSource($fileName);
	}

	public function clearData()
	{
		$sql = "DELETE FROM users";
		$this->db->exec($sql);
	}

}

class Person{
	
	var $name;
	var $last_name;
	var $address;
	var $cellphone;
	var $telephone;
	var $avatar;
}

$time1 = time();

$objImport = new Import();
//$objImport->createTable();
$objImport->importData("source/users.csv");

//$objImport->showData();
//$objImport->clearData();

//print_r(date("H:i:s"));
$time2 = time();
echo "process in ".($time2 - $time1)." seg";
