<?php


class Import{

	var $db;

	private function conection()
	{
		$this->db = require __DIR__.'../db/db.php';
	}

	public function listPersons()
	{
		$orderColumn = $page = $textFilter = $typeFilter = $orderType = "";
		$pageTotal = 0;

		if(isset($_GET['orderColumn']))
			$orderColumn = $_GET['orderColumn'];
		if(isset($_GET['page']))
			$page = $_GET['page'];
		if(isset($_GET['textFilter']))
			$textFilter = trim($_GET['textFilter']);
		if(isset($_GET['typeFilter']))
			$typeFilter = $_GET['typeFilter'];
		if(isset($_GET['orderType']))
			$orderType = $_GET['orderType'];

		$contacts = $this->getPersons($orderColumn, $orderType, $page, $typeFilter, $textFilter, $pageTotal);

		$data = array();
		$data["contacts"] = $contacts;
		$data["page"] = $page;
		$data["pageTotal"] = $pageTotal;

		header("Content-type:application/json");
		echo json_encode($data);
	}

	private function getPersons($orderColumn = "name", $orderType = "ASC", $page = 1, $typeFilter = "", $textFilter = "", &$pageTotal = 0)
	{	
		$this->conection();

		if($orderColumn == "")
			$orderColumn = "name";
		if($orderType == "")
			$orderType = "ASC";
		if($page == "")
			$page = 1;

		$sql = "SELECT * FROM users";
		$sqlCount = "SELECT count(*) as 'count' FROM users";
		
		if($typeFilter != "" and $textFilter != ""){
			$sql .= " where $typeFilter like '%$textFilter%'";
			$sqlCount .= " where $typeFilter like '%$textFilter%'";
		}

		$sql .= " order by $orderColumn $orderType";
		$sqlCount .= " order by $orderColumn $orderType";

		$sql .= " LIMIT 10 OFFSET ".($page * 10 - 10);

		$result = $this->db->query($sql);		
		$resultCount = $this->db->query($sqlCount);

		$number = $resultCount->fetch();
		$pageTotal = ceil(intval($number['count'])/10);

		$rows = $result->fetchAll();

		return $rows;
	}

}

$objImport = new Import();
$objImport->listPersons();
